﻿using System;
using System.Windows;

namespace InspectionApp
{
    /// <summary>
    /// Interaction logic for CustomWindow.xaml
    /// </summary>
    public partial class CustomWindow : Window
    {//Used as a common Window that can used to show an image as well as any piece of text
        public CustomWindow()
        {
            InitializeComponent();
            textblock1.TextWrapping = TextWrapping.Wrap;
            
        }
        public CustomWindow(String text)
        {
            InitializeComponent();
            textblock1.Text = text;
            textblock1.TextWrapping = TextWrapping.Wrap;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public String ResponseText
        {
            get { return textbox1.Text; }
            set { textbox1.Text = value; }
        }
    }
}
