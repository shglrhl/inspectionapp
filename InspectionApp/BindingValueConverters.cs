﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace InspectionApp
{
    [ValueConversion(typeof(double), typeof(double))]
    public class DoubleLinearConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is double))
            {
                return DependencyProperty.UnsetValue;
            }

            if (parameter == null)
            {
                ThrowParameterError("The converter parameter is null.");
            }

            if (!(parameter is object[]))
            {
                ThrowParameterError("The converter parameter is not an array of objects.");
            }

            var parameters = (object[])parameter;

            if (parameters.Length < 4)
            {
                ThrowParameterError("The converter parameter array does not have at least 4 elements.");
            }

            if (!(parameters[0] is double))
            {
                ThrowParameterError("Converter parameter[0] is not of type 'double'.");
            }
            if (!(parameters[1] is double))
            {
                ThrowParameterError("Converter parameter[1] is not of type 'double'.");
            }
            if (!(parameters[2] is double))
            {
                ThrowParameterError("Converter parameter[2] is not of type 'double'.");
            }
            if (!(parameters[3] is double))
            {
                ThrowParameterError("Converter parameter[3] is not of type 'double'.");
            }

            var X = (double)value;

            var X1 = (double)parameters[0];
            var X2 = (double)parameters[1];
            var Y1 = (double)parameters[2];
            var Y2 = (double)parameters[3];
            var format = (string)parameters[4];

            var M = (Y2 - Y1) / (X2 - X1); // slope
            var B = Y1 - (M * X1);        // y intercept

            var Y = (M * X) + B;

            return Y;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        static private void ThrowParameterError(string s)
        {
            var msg =
                "DoubleLinearFormatConverter error:  " +
                s +
                "  This converter requires an array object paramter that is an object array containing " +
                "four doubles in paramter[0,1,2,3].";

            throw new ArgumentException(s);
        }
    }

    /// <summary>Performs a simple linear conversion on a double and provides a formatted string of the result.</summary>
    /// <remarks>
    /// This converter takes five parameters that are passed in the Convert functions 'parameter' value as an array of
    /// objects.   For example, the arrya values to convert an input value from 0 to 100 to values from 0.1 to 5.0 then
    /// format the result string with F3 is done like this:
    /// 
    /// <code>
    ///    <Window.Resources>
    ///        <x:Array Type="{x:Type sys:Object}" x:Key="SizeSliderConversionParameters">
    ///
    ///            <sys:Double>0/sys:Double>     <!-- X1 -->
    ///            <sys:Double>100</sys:Double>  <!-- X2 -->
    ///
    ///            <sys:Double>0.1</sys:Double>  <!-- Y1 -->
    ///            <sys:Double>5.0</sys:Double>  <!-- Y2 -->
    ///
    ///            <sys:String>F3</sys:String>
    ///
    ///     </x:Array>
    /// </code>
    /// </remarks>
    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleLinearFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is double))
            {
                return DependencyProperty.UnsetValue;
            }

            if (parameter == null)
            {
                ThrowParameterError("The converter parameter is null.");
            }

            if (!(parameter is object[]))
            {
                ThrowParameterError("The converter parameter is not an array of objects.");
            }

            var parameters = (object[])parameter;

            if (parameters.Length < 5)
            {
                ThrowParameterError("The converter parameter array does not have at least 5 elements.");
            }

            if (!(parameters[0] is double))
            {
                ThrowParameterError("Converter parameter[0] is not of type 'double'.");
            }
            if (!(parameters[1] is double))
            {
                ThrowParameterError("Converter parameter[1] is not of type 'double'.");
            }
            if (!(parameters[2] is double))
            {
                ThrowParameterError("Converter parameter[2] is not of type 'double'.");
            }
            if (!(parameters[3] is double))
            {
                ThrowParameterError("Converter parameter[3] is not of type 'double'.");
            }
            if (!(parameters[4] is string))
            {
                ThrowParameterError("Converter parameter[4] is not of type 'string'.");
            }

            var X = (double)value;

            var X1 = (double)parameters[0];
            var X2 = (double)parameters[1];
            var Y1 = (double)parameters[2];
            var Y2 = (double)parameters[3];
            var format = (string)parameters[4];

            var M = (Y2 - Y1) / (X2 - X1); // slope
            var B = Y1 - (M * X1);         // y intercept

            var Y = (M * X) + B;

            return Y.ToString(format);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        static private void ThrowParameterError(string s)
        {
            var msg =
                "DoubleLinearFormatConverter error:  " +
                s +
                "  This converter requires an array object paramter that is an object array containing " +
                "four doubles in paramter[1,2,3,4] and a string in parameter[5].";

            throw new ArgumentException(s);
        }
    }

    /// <summary>String Formats a double</summary>
    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is double) || parameter == null || !(parameter is string))
            {
                return DependencyProperty.UnsetValue;
            }

            var Y = (double)value;

            return Y.ToString((string)parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}