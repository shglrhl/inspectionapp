﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace InspectionApp
{
    internal class Utilities
    {
        public static void SavePartImage(string selectedItemClass, Image img)
        {
            //Converting image to a MemoryStream so that it can be stored in the database as an Image.
            var img1 = img.Source as RenderTargetBitmap;
            var encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(img1));

            using (var memorystream = new MemoryStream())
            {
                encoder.Save(memorystream);
                memorystream.Position = 0;
                var image = new byte[memorystream.Length];
                memorystream.Read(image, 0, Convert.ToInt32(memorystream.Length)); //Reading image into the memorystream

                try
                {
                    using (
                        var conn =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings[
                                    "InspectionApp.Properties.Settings.PolyconInjectionConnectionString"]
                                    .ConnectionString))
                    {
                        conn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandText =
                                "SELECT count(*) from [INSPECTION_ItemClass_IMAGES] WHERE [ItemClass] = @ItemClass";
                            cmd.Parameters.AddWithValue("@ItemClass", selectedItemClass);
                            var count = (int) cmd.ExecuteScalar(); //Checking if an Image already exists for the Selected Item Class

                            cmd.CommandText = count > 0 ? "UPDATE [INSPECTION_ItemClass_IMAGES] SET [ItemClass_Image] = @Image WHERE [ItemClass] = @ItemClass" : "INSERT into [INSPECTION_ItemClass_IMAGES] ([ItemClass], [ItemClass_Image]) VALUES (@ItemClass, @Image)";
                            // ^ Checking we need to update the database or insert into it depending on if Image already exists or not
                            cmd.Parameters.Add("@Image", SqlDbType.Image);
                            cmd.Parameters["@Image"].Value = image;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Database connection error. Not saved. Error:" + ex.Message);
                }
            }
        }


        public static RenderTargetBitmap DrawImage(BitmapImage bi, ObservableCheckList checklist,
            string selectedItemClass) //Used to draw and redraw the image and then return it
        {
            var drawingVisual = new DrawingVisual();
            var drawingContext = drawingVisual.RenderOpen();

            drawingContext.DrawImage(bi, new Rect(0.0, 0.0, bi.Width, bi.Height)); //Drawing the default part image.

            foreach (var i in checklist) //For each INSPECTION_ALERT item in the checklist, a number point is added 
                                        //to the part image corresponding to the priority of the INSPECTION_ALERT item.  
            {
                try
                {
                    if (i.ItemClass == selectedItemClass)
                    {
                        var coordinates = i.Coordinates.Split(';'); //Getting the individual coordinates 
                        foreach (var coordinate in coordinates)
                        {
                            if (coordinate != "")
                            {
                                var subCoordinate = coordinate.Split(','); //Splitting the width and height of the coordinate
                                var name = string.Format("pack://application:,,,/Images/{0}.bmp", i.Priority); //Getting the string pointing to the number point image
                                var overlayImage = new BitmapImage(new Uri(name)); //Getting the number point image itself
                                drawingContext.DrawImage(overlayImage,
                                    new Rect(Convert.ToDouble(subCoordinate[0]), Convert.ToDouble(subCoordinate[1]),
                                        overlayImage.PixelWidth, overlayImage.PixelHeight)); //Drawing the numbered point image over part iamge at the designated coordinates
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            drawingContext.Close();  //Finalizing the drawing context
            var mergedImage = new RenderTargetBitmap(Convert.ToInt32(bi.Width), Convert.ToInt32(bi.Height), 96, 96,
                PixelFormats.Pbgra32);
            mergedImage.Render(drawingVisual); //Rendering the drawingvisual into an image which is then returned
            return mergedImage;
        }
    }
}