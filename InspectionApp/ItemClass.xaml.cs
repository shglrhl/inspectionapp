﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using InspectionApp.Linq2SQL;
using InspectionApp.PolyconInjectionDataSet2TableAdapters;

namespace InspectionApp
{
    /// <summary>
    /// Interaction logic for ItemClass.xaml
    /// </summary>
    
    
    public partial class ItemClass : Window
    {
        private static readonly DataClasses3DataContext DataDc3 = new DataClasses3DataContext();

        private ObservableCheckList3 _itemClasses;
        private List<ImageDetails> images_global;

        public ItemClass()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            /////Initializing dropdown
            var polyconInjectionDataSet2 = ((PolyconInjectionDataSet2)(FindResource("polyconInjectionDataSet2")));
            // Load data into the table PartNumber_ItemClass. You can modify this code as needed.
            var polyconInjectionDataSet2PartNumber_ItemClassTableAdapter = new PartNumber_ItemClassTableAdapter();
            polyconInjectionDataSet2PartNumber_ItemClassTableAdapter.Fill(polyconInjectionDataSet2.PartNumber_ItemClass);
            var partNumber_ItemClassViewSource = ((CollectionViewSource)(FindResource("partNumber_ItemClassViewSource")));
            partNumber_ItemClassViewSource.View.MoveCurrentToFirst();
            
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            
            worker.DoWork += worker_DoWork; 
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();

            _itemClasses = new ObservableCheckList3(DataDc3); //Getting the itemclasses from the DB
            part_NumberComboBox.SelectionChanged += part_NumberComboBox_SelectionChanged;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                part_NumberComboBox.IsEnabled = true;
                save.IsEnabled = true;
                reset.IsEnabled = true;
                imageview.ItemsSource = images_global;
                ProgressBar.Value = 0;
                ProgressBar.IsEnabled = false;
                progress_label.Content = "Images Loaded";
                progress_label.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Background Worker error.");
            } 
        }


        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress_label.Content = e.ProgressPercentage + "%";
            ProgressBar.Value = e.ProgressPercentage;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            ((BackgroundWorker) sender).ReportProgress(5);
            /////Initializing images
            var root = @"\\ply-3214\g\Mold Part Images";
            string[] supportedExtensions = { ".bmp" };
            var files = Directory.GetFiles(root, "*.*").Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower()));

            var images = new List<ImageDetails>();
            int totalfiles;
            int currentfile = 0;
            int nothing;
            foreach (var file in files)
            {
                totalfiles = files.Count();
                currentfile ++;
                double prcnt = (currentfile/(double)totalfiles)*100.0000;
                double chk = Math.IEEERemainder(prcnt, 10);
                if (chk == 0.0)
                    ((BackgroundWorker)sender).ReportProgress(Convert.ToInt32(prcnt));

                var id = new ImageDetails
                {
                    Name = Path.GetFileNameWithoutExtension(file)
                };

                var img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.UriSource = new Uri(file, UriKind.Absolute);
                img.EndInit();

                id.Image = img;

                var bi = new BitmapImage();
                bi.BeginInit();
                bi.DecodePixelWidth = 150;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.UriSource = new Uri(file, UriKind.Absolute);
                bi.EndInit();

                id.ThumbnailImage = bi;
                id.Image.Freeze();
                id.ThumbnailImage.Freeze();
                images.Add(id);
            }
            images_global = images;
        }

        private void part_NumberComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            
            var selectedItem = (PolyconInjectionDataSet2.PartNumber_ItemClassRow)((DataRowView)part_NumberComboBox.SelectedItem).Row;
            if (string.IsNullOrEmpty(selectedItem.Part_Number)) return;
            var item = _itemClasses.FirstOrDefault(x => x.Part_Number == selectedItem.Part_Number);
            if (Equals(item, null))
            {
                MessageBox.Show(
                    @"Part Number not found, for PartNumber: " + selectedItem.Part_Number + " & ItemClass: " + selectedItem.ItemClass + ".");
                return;
            }
            var itemclass = item.ItemClass.Substring(1, 2);

            var assignedImage = imageview.Items.Cast<ImageDetails>().FirstOrDefault(x => (x).Name.Contains(itemclass));
            if(Equals(assignedImage,null))
            {
                MessageBox.Show(
                    @"No Part Image found for PartNumber: " + selectedItem.Part_Number + " & ItemClass: " + selectedItem.ItemClass + ". " + Environment.NewLine + @"Please contact the IT Systems Analyst to porvide all new part images and paste them in \\PLY-3214\G\Mold Part Images location.");
                return;
            }
            var index = imageview.Items.IndexOf(assignedImage);
            imageview.SelectedIndex = index;

            imageview.UpdateLayout();
            imageview.ScrollIntoView(imageview.Items[index]);
            var lvi = imageview.ItemContainerGenerator.ContainerFromIndex(index) as ListViewItem;
            lvi.Focus();

            SelectedImage.Source = assignedImage.Image;


        }

        private void imageview_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var imagedets = ((ImageDetails)((ListView) sender).SelectedItem);
            SelectedImage.Source = imagedets.Image;

            var Item = _itemClasses.FirstOrDefault(x => x.Part_Number.Trim() == ((PolyconInjectionDataSet2.PartNumber_ItemClassRow)((DataRowView)part_NumberComboBox.SelectedItem).Row).Part_Number.ToString().Trim());
            var index = _itemClasses.IndexOf(Item);
            _itemClasses[index].ItemClass = "0" + imagedets.Name + "1";
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataDc3.SubmitChanges();
                MessageBox.Show("Part Images & ItemClasses updated.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error encoutered. Error :" + ex.Message);
            }
            
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            var changes = DataDc3.GetChangeSet(); //Gets all changes made after last save
            var updatedtables = new List<ITable>(); //Gets all updates
            foreach (var updates in changes.Updates)
            {
                var tbl = DataDc3.GetTable(updates.GetType());
                if (updatedtables.Contains(tbl)) continue;
                updatedtables.Add(tbl);
                DataDc3.Refresh(RefreshMode.OverwriteCurrentValues, tbl); //Overwrites any updates to the previously saved state
            }

            _itemClasses = null;
            _itemClasses = new ObservableCheckList3(DataDc3); //Refreshes the _checklist variable by populating it again from the DB
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }

    public class ObservableCheckList3 : ObservableCollection<PartNumber_ItemClass>
    {
        public ObservableCheckList3(DataClasses3DataContext datadc)
        {
            foreach (var pi in datadc.PartNumber_ItemClasses)
            {
                Add(pi);
            }
        }
    }

    public class ImageDetails
    {
        public string Name { get; set; }

        public BitmapImage Image { get; set; }

        public BitmapImage ThumbnailImage { get; set; }

    }
}
