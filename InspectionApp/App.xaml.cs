﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace InspectionApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var ex = e.Exception;
            var ex_inner = ex.InnerException;
            var msg = ex.Message + "\n\n" + ex.StackTrace + "\n\n" +
                "Inner Exception:\n" + ex_inner.Message + "\n\n" + ex_inner.StackTrace;
            MessageBox.Show(msg, "Application Halted!", MessageBoxButton.OK);
            e.Handled = true;
            Application.Current.Shutdown();
        }
    }
}
