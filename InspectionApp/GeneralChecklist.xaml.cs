﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InspectionApp.Linq2SQL;

namespace InspectionApp
{
    /// <summary>
    /// Interaction logic for GeneralChecklist.xaml
    /// </summary>
    public partial class GeneralChecklist
    {
        private static readonly DataClasses2DataContext DataDc2 = new DataClasses2DataContext();

        private ObservableCheckList2 _generalChecklist;

        public GeneralChecklist()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            _generalChecklist = new ObservableCheckList2(DataDc2);//Loading Bindings
            iNSPECTION_GENERAL_CHECKLISTListView.ItemsSource = _generalChecklist; //Setting ItemSource
        }

        private void TextBox_MouseDown(object sender, MouseButtonEventArgs e)
        {//Used to select the iNSPECTION_GENERAL_CHECKLIST item whose textbox the user clicks on.
            iNSPECTION_GENERAL_CHECKLISTListView.SelectedItem = null;
            var newSelectedItem = (sender as TextBox).TryFindParent<ListViewItem>();
            newSelectedItem.IsSelected = true;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {//New INSPECTION_GENERAL_CHECKLIST
            var newInspection = new INSPECTION_GENERAL_CHECKLIST
            {
                Description = "Enter Description",
                ShortName = "Enter Short Name"
            };


            DataDc2.INSPECTION_GENERAL_CHECKLISTs.InsertOnSubmit(newInspection);
            _generalChecklist.Add(newInspection);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (iNSPECTION_GENERAL_CHECKLISTListView.SelectedItem != null)
            {
                DataDc2.INSPECTION_GENERAL_CHECKLISTs.DeleteOnSubmit((INSPECTION_GENERAL_CHECKLIST)iNSPECTION_GENERAL_CHECKLISTListView.SelectedItem);

                _generalChecklist.Remove((INSPECTION_GENERAL_CHECKLIST)iNSPECTION_GENERAL_CHECKLISTListView.SelectedItem);
            }
            else
            {
                MessageBox.Show("No item selected. Nothing to delete.");
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            DataDc2.SubmitChanges();
            DialogResult = true;
        }
    }

    public class ObservableCheckList2 : ObservableCollection<INSPECTION_GENERAL_CHECKLIST>
    {
        public ObservableCheckList2(DataClasses2DataContext dataDc)
        {
            foreach (var alert in dataDc.INSPECTION_GENERAL_CHECKLISTs)
            {
                Add(alert);
            }
        }
    }//Allows LINQ2SQL to work with Bindings
}
