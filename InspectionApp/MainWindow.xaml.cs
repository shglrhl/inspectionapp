﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using ImageMagick;
using InspectionApp.DDCMouldDataSetTableAdapters;
using InspectionApp.Linq2SQL;
using InspectionApp.PolyconInjectionDataSet2TableAdapters;
using InspectionApp.PolyconInjectionDataSetTableAdapters;
using Microsoft.Office.Interop.Excel;
using SimpleLogger;
using Action = System.Action;
using Application = System.Windows.Application;
using TextBox = System.Windows.Controls.TextBox;

namespace InspectionApp
{
    public partial class MainWindow
    {
        private static readonly DataClasses1DataContext DataDc = new DataClasses1DataContext(); //Used for LINQ2SQL to be functional.
        
        private string _selectedItemClass = "";

        private ObservableCheckList _checklist;

        private ObservableCollection<string> _qualityAlerts = new ObservableCollection<string>();

        public string UserSignedIn { get; set; }

        public bool SpinStatus = false;

        public MainWindow()
        {
            
            InitializeComponent();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BizzySpinner.IsEnabled = false;
            var spinstart = new Thread(Start_Spinning);                                     //Start Spinning Spinner
            spinstart.Start();

            if (!Directory.Exists(@"\\PLY-3214\G\Mold Part Images"))                        //Checking if the Images folder exists and is accessible
            {
                MessageBox.Show(@"Cannot access the Part Images location: \\PLY-3214\G\ Ensure that the remote location with Part Images is accessible and turned on.");
                return;
            }
            
            SimpleLog.SetLogFile(AppDomain.CurrentDomain.BaseDirectory,"Log_",writeText:false);           //Setting up LOG file for error logging 
            var polyconInjectionDataSet = ((PolyconInjectionDataSet)(FindResource("polyconInjectionDataSet")));         //Loading all the part numbers available.

            var polyconInjectionDataSetPartsTableAdapter = new PARTSTableAdapter();         // Load data into the PARTS table .
            polyconInjectionDataSetPartsTableAdapter.Fill(polyconInjectionDataSet.PARTS);   // Load data into the PARTS table .
                                                                                            //
            //var pArtsViewSource = ((CollectionViewSource)(FindResource("pARTSViewSource")));//Binding Parts to the ViewSource
            //pArtsViewSource.View.MoveCurrentToFirst();                                      //Binding Parts to the ViewSource

            


            _checklist = new ObservableCheckList(DataDc);               //Loading Data from DataContext
            listview1.ItemsSource = _checklist;                         //Setting viewsource for the listview to be bound to.
            listview1.IsEnabled = false;                                
            _qualityAlerts.Add("Custom  Note");   
            PartNumberComboBox.IsEnabled = false;
            WindowState = WindowState.Maximized;                        //FullScreen
            var spinstop = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
            spinstop.Start();
        }

        

        public void Start_Spinning()
        {
            if (SpinStatus) return;
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => BizzySpinner.Spin = true));
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => BizzySpinner.IsEnabled = true));
            SpinStatus = true;
        }

        public void Stop_Spinning()
        {
            if (!SpinStatus) return;
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => BizzySpinner.Spin = false));
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => BizzySpinner.IsEnabled = false));
            SpinStatus = false;
        }

        private void partNumberComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var spinstart = new Thread(Start_Spinning) {Priority = ThreadPriority.Highest};
            spinstart.Start();                                                                  //Start Spinning Spinner
            try
            {
                listview1.IsEnabled = true;                                                     //Enable listview as soon as PartNumber is selected.
                if (((ComboBox) sender).SelectedIndex == 0)
                {
                    var spinstop1 = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
                    spinstop1.Start();
                    return;
                }
                var ddc = new DDCMouldDataSet();
                var da = new tbl_INJECTION_PARTS_DETAILSTableAdapter();
                da.Fill(ddc.tbl_INJECTION_PARTS_DETAILS);                                       //Filling INJECTION PARTS DETAILS with data (To look for Part Images using ItemClass)
                var partnumber = ((DataRowView) PartNumberComboBox.Items.CurrentItem).Row.ItemArray[0].ToString();//Getting Selected PartNumber
                var filterstring = "PartNumber = '" + partnumber + "'";                         //Will allow us to filter out the ListItem belonging to the selected PartNumber
                var foundrows = ddc.tbl_INJECTION_PARTS_DETAILS.Select(filterstring);           //Filtering out the Listitems
                if (foundrows.Length <= 0)                                                      //Checking if any Items exist for the particular partnumber
                {
                    MessageBox.Show("Part Number doesn't contain any parts");
                    var spinstop2 = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
                    spinstop2.Start();
                    return;
                }
                var polyconInjectionDataSet2 = new PolyconInjectionDataSet2();                  // Load data into the table PartNumber_ItemClass. You can modify this code as needed.
                var polyconInjectionDataSet2PartNumberItemClassTableAdapter = new PartNumber_ItemClassTableAdapter();       //Loading ItemClasses to get Part Images
                polyconInjectionDataSet2PartNumberItemClassTableAdapter.Fill(polyconInjectionDataSet2.PartNumber_ItemClass);//Loading ItemClasses to get Part Images

                var item = polyconInjectionDataSet2.PartNumber_ItemClass.FirstOrDefault(x => x.Part_Number.Trim() == partnumber.Trim()); //Searching for the Selected PartNumber's ItemClass
                if (Equals(item, null))                                                 //If no Selected PartNumber's ItemClass is not found
                {
                    MessageBox.Show("No ItemClass associated with this PartNumber.");
                    return;
                }
                if (Equals(item.ItemClass,null))                                        //If Selected PartNumber has no ItemClass associated with it
                {
                    MessageBox.Show("No ItemClass associated with this PartNumber.");
                    return;
                }
                var itemClass = item.ItemClass.Substring(1, 2);                         //Setting ItemClass to the letters of the ItemClass that matter
            
                
                _selectedItemClass = itemClass;                                         //Setting the global selectedItemClass
                label1.Content = itemClass;

            
                var view = CollectionViewSource.GetDefaultView(listview1.ItemsSource);  //Getting defaultview to Filter the InspectionAlerts
                view.Filter = x =>
                {
                    var i = x as INSPECTION_ALERT;
                    return string.Equals(i.ItemClass, _selectedItemClass);
                };                                                                      //Filtering INSPECTION_ALERTS according to the selected PartNumber's ItemClass

                try
                {
                    var bi = new BitmapImage(new Uri(@"\\PLY-3214\G\Mold Part Images\" + itemClass + ".bmp")); //Getting Part Image
                    image1.Source = null;                                               //Clearing old source
                    image1.Source = Utilities.DrawImage(bi, _checklist, _selectedItemClass); //Setting new image for the Selected PartNumber
                
                }
                catch (Exception)                                                       //If part image not loaded. Throws error message, and returns.
                {
                    MessageBox.Show("Part Number doesn't have a corresponding Image.");
                    var spinstop4 = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
                    spinstop4.Start();
                    return;
                }
                try
                {
                    if(_qualityAlerts.Count < 2)                                        // Should be less than 2 because it already has "Custom Note"
                    {                                                                   //Searching the Quality Alerts folder and filling the dropdown with file names.
                        var files =  Directory.EnumerateFiles(@"F:\ISO-TS 16949\Moulding Quality Alerts\2016 Quality Alerts\","*.*",SearchOption.AllDirectories).Where(x => (x.Contains(".xls") && x.Contains("QA")) || (x.Contains(".doc") && x.Contains("MB")) || (x.Contains(".pdf"))); 
                        foreach (var file in files)                                     //Go through each file name to add it to the dropdown
                        {
                            _qualityAlerts.Add(file.Substring(file.LastIndexOf("\\", StringComparison.Ordinal)+1)); //Gets file name from the path and adds it.
                        }
                        _qualityAlerts = new ObservableCollection<string>(_qualityAlerts.OrderBy(i => i.ToString()).ToList()); //Sorting the order
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot access location :F:\\ISO-TS 16949\\Moulding Quality Alerts\\ Ensure the network location is accessible.");
                }
                var spinstop = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
                spinstop.Start();
            }
            catch (Exception ex)
            {
                {
                    MessageBox.Show("Error encountered. Error:" + ex.Message);
                }
            }
            
        }
        
        private void button_new_Click(object sender, RoutedEventArgs e)
        {
            if (PartNumberComboBox.SelectedIndex != 0) //Ensuring that a part number is selected before adding a new INSPECTION_ALERT
            {
                var newInspection = new INSPECTION_ALERT //Creating new INSPECTION_ALERT
                {
                    Description = "Enter Description",
                    ItemClass = _selectedItemClass,
                    Priority = 1,
                    Active = true
                };


                DataDc.INSPECTION_ALERTs.InsertOnSubmit(newInspection); //INSPECTION_ALERT will be inserted to the database upon submit
                _checklist.Add(newInspection);//Adding the INSPECTION_ALERT to _checklist to allow manipulation on submit
                
            }
            else
            {
                MessageBox.Show("Please select a valid PartNumber first");
            }
        }

        private void button_delete_Click(object sender, RoutedEventArgs e)
        {
            if(PartNumberComboBox.SelectedIndex != 0)
            {
                if (listview1.SelectedItem != null) //To ensure an item is indeed selected
                {
                    DataDc.INSPECTION_ALERTs.DeleteOnSubmit((INSPECTION_ALERT)listview1.SelectedItem); //Setting selected INSPECTION_ALERT to delete on submit

                    _checklist.Remove((INSPECTION_ALERT)listview1.SelectedItem); 

                    var bi = new BitmapImage(new Uri(@"\\PLY-3214\G\Mold Part Images\" + _selectedItemClass + ".bmp")); //Getting image again to Redraw it
                    image1.Source = null;
                    image1.Source = Utilities.DrawImage(bi, _checklist, _selectedItemClass); //Redrawing image to ensure the previously removed INSPECTION_ALERT is gone from the image too
                }
                else
                {
                    MessageBox.Show("No item selected. Nothing to delete.");
                }
            }
            else
            {
                MessageBox.Show("Please select a valid PartNumber first");
            }            
        }

        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            var spinstart = new Thread(Start_Spinning);
            spinstart.Start();


            Utilities.SavePartImage(_selectedItemClass, image1); //To save the Part Image to the database


            var files = Directory.EnumerateFiles(@"F:\ISO-TS 16949\Moulding Quality Alerts\2016 Quality Alerts\", "*.*", SearchOption.AllDirectories).Where(x => (x.Contains(".xls") && x.Contains("QA")) || (x.Contains(".doc") && x.Contains("MB")) || (x.Contains(".pdf")));  //GEtting a list of all QA's
            var view = CollectionViewSource.GetDefaultView(listview1.ItemsSource);
            var CS1 = DataDc.GetChangeSet().Updates.OfType<INSPECTION_ALERT>();
            foreach (var iA in CS1) //Getting all the now modified, INSPECTION_ALERT items inside the view
            {
                if (iA.MoreInfo == "Custom  Note") continue; //Checking if the MoreInfo is a "Custom Note" text or an excel QA sheet.
                var file = files.First(x => x.Contains(iA.MoreInfo.ToString())); //Getting the name of the file if it indeed is an excel file
                
                
                try
                {
                    var image = GetImage(file);
                    if (!Equals(image, null))
                    {
                        iA.MoreInfo_Image = image;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not get MQA Image. ");
                    SimpleLog.Log("Next Error occurs while converting MQA's to image");
                    SimpleLog.Log(ex);
                }
            }
            var CS2 = DataDc.GetChangeSet().Inserts.OfType<INSPECTION_ALERT>();
            foreach (var iA in CS2) //Getting all the now modified, INSPECTION_ALERT items inside the view
            {
                if (iA.MoreInfo == "Custom  Note") continue; //Checking if the MoreInfo is a "Custom Note" text or an excel QA sheet.
                var file = files.First(x => x.Contains(iA.MoreInfo.ToString())); //Getting the name of the file if it indeed is an excel file


                try
                {
                    var image = GetImage(file);
                    if (!Equals(image, null))
                    {
                        iA.MoreInfo_Image = image;
                    }
                    else
                    {
                        MessageBox.Show("Couldn't create QA image for " + _selectedItemClass + " MQA:" + iA.MoreInfo);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not get MQA Image. ");
                    SimpleLog.Log("Next Error occurs while converting MQA's to image");
                    SimpleLog.Log(ex);
                }
            }
            MessageBox.Show("Saved");

            if (PartNumberComboBox.SelectedIndex != 0) 
            {
                DataDc.SubmitChanges(); //If a PartNumber is selected, then Submit the changes that were made after the last save.
            }
            else
            {
                MessageBox.Show("Please select a valid PartNumber first");
                var spinstop1 = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
                spinstop1.Start();
                return;
            }
            var spinstop2 = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
            spinstop2.Start();
        }

        private byte[] GetImage(string file)
        {
            BitmapSource bs = null;
            if (file.Contains(".xls"))
            {
                var xlApp = new Microsoft.Office.Interop.Excel.Application {DisplayAlerts = false};
                    //So it does not display any microsoft excel dialog boxes which would interfere with and pause the process
                Workbook xlWb = null;
                try
                {
                    //Opening the selected excel sheet
                    xlWb = xlApp.Workbooks.Open(file,
                        false, //updatelinks
                        true, //readonly
                        Missing.Value, //format
                        Missing.Value, //Password
                        Missing.Value, //writeResPass
                        true, //ignoreReadOnly
                        Missing.Value, //origin
                        Missing.Value, //delimiter
                        true, //editable
                        Missing.Value, //Notify
                        Missing.Value, //converter
                        Missing.Value, //AddToMru
                        Missing.Value, //Local
                        Missing.Value);
                    Worksheet xlS = xlWb.ActiveSheet;
                    const string startRange = "A1";
                    var endrange = xlS.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Missing.Value);
                    var range = xlS.Range[startRange, endrange];
                    //Selecting the range that contains all the data of the excel sheet
                    range.Rows.AutoFit();
                    range.Columns.AutoFit();
                    range.Copy();
                    //All the data from inside that range is then copied to the clipboard as an image.
                    bs = Clipboard.GetImage(); //Image is recieved from the clipboard
                    Clipboard.Clear();
                }
                catch (Exception ex)
                {
                    xlWb.Close(false, Missing.Value, Missing.Value);
                    xlApp.Quit();
                    xlApp = null;
                    SimpleLog.Log("Next Error occurs while converting Excel doc to image");
                    SimpleLog.Log(ex);
                    MessageBox.Show("Could not convert " + file + " to image.");
                    return null; // continue;
                }
            }
            else if (file.Contains(".doc"))
            {
                var app = new Microsoft.Office.Interop.Word.Application {Visible = true};

                var doc = app.Documents.Open(file);
                try
                {
                    using (var images = new MagickImageCollection())
                    {
                        var window = doc.ActiveWindow;
                        var pane = window.ActivePane;
                        for (var i = 1; i <= pane.Pages.Count - 1; i++)
                        {
                            var page = pane.Pages[i];
                            var bits = (byte[]) page.EnhMetaFileBits;
                            try
                            {
                                using (var ms = new MemoryStream(bits))
                                {
                                    var image = new MagickImage(ms) {Quality = 20};
                                    image.Resize(new Percentage(20));
                                    images.Add(image);
                                }
                            }
                            catch (Exception ex)
                            {
                                // ignored
                            }
                        }
                        ;
                        using (var result = images.AppendVertically())
                        {
                            result.Write(@"C:\Users\rsehgal\Desktop\TestImages\abc.png");
                            bs = result.ToBitmapSource();
                        }
                    }
                }
                catch (Exception ex)
                {
                    doc.Close(Type.Missing, Type.Missing, Type.Missing);
                    app.Quit(Type.Missing, Type.Missing, Type.Missing);
                    SimpleLog.Log("Next Error occurs while converting DOC to image");
                    SimpleLog.Log(ex);
                    MessageBox.Show("Could not convert " + file + " to image.");
                    return null; // continue;
                }
            }
            else if (file.Contains(".pdf"))
            {
                try
                {
                    using (var images = new MagickImageCollection())
                    {
                        // Add all the pages of the pdf file to the collection
                        var rasterizer = new GhostscriptRasterizer();
                        var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                        var vesion =
                            new GhostscriptVersionInfo(new Version(0, 0, 0), path + @"\gsdll32.dll",
                                string.Empty, GhostscriptLicense.GPL);

                        rasterizer.Open(file, vesion, false);
                        for (var pageNumber = 1; pageNumber <= rasterizer.PageCount; pageNumber++)
                        {
                            var img = new MagickImage(new Bitmap(rasterizer.GetPage(96, 96, pageNumber)));
                            images.Add(img);
                        }

                        // Create new image that appends all the pages horizontally

                        // Create new image that appends all the pages horizontally
                        using (var result = images.AppendVertically() /*images.Mosaic()*/)
                        {
                            bs = result.ToBitmapSource();
                        }
                    }
                }
                catch (Exception ex)
                {
                    SimpleLog.Log("Next Error occurs while converting PDF to image");
                    SimpleLog.Log(ex);
                    MessageBox.Show("Could not convert " + file + " to image.");
                    return null; // continue;
                }
            }
            else
            {
                MessageBox.Show("Application does not support conversion of " + file + " to image.");
                return null; // return;
            }
            if (bs != null)
            {
                // Converting the image to a memory stream so it can be stored in the database as an image
                var encoder = new JpegBitmapEncoder {QualityLevel = 100};
                byte[] bit = null;
                using (var stream = new MemoryStream())
                {
                    encoder.Frames.Add(BitmapFrame.Create(bs));
                    encoder.Save(stream);
                    bit = stream.ToArray();
                    stream.Close();
                }
                return bit; // return bit; //Image stored a an array of bits 
            }
            return null;
        }

        private void image1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //Used to mark points on the Part Image where we will place the number pointers.
            var frameworkelement = (FrameworkElement)sender;
            var image = (IInputElement)frameworkelement.FindName("image1");
            var point = e.GetPosition(image); //Gets the position of the click
            try
            {
                ((INSPECTION_ALERT)listview1.SelectedItem).Coordinates += Math.Round(((point.X / image1.Width) - 0.02) * image1.Source.Width, 2) + 
                    "," + Math.Round(((point.Y / image1.Height) - 0.05) * image1.Source.Height, 2) + ";"; //Adding the coordinate to the string insde the textbox to be used later.
            }
            catch(Exception)
            {
                MessageBox.Show("Please select something from the list.");
                return;
            }


            var bi = new BitmapImage(new Uri(@"\\PLY-3214\G\Mold Part Images\" + _selectedItemClass + ".bmp")); //Getting the image 
            image1.Source = null;
            image1.Source = Utilities.DrawImage(bi, _checklist, _selectedItemClass); //Redrawing the image
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        //Resets the data to a previously saved state.
        {
            var spinstart = new Thread(Start_Spinning);                                     //Start Spinning Spinner
            spinstart.Start();
            var changes = DataDc.GetChangeSet(); //Gets all changes made after last save
            foreach (var inserts in changes.Inserts)
            {
                DataDc.GetTable(inserts.GetType()).DeleteOnSubmit(inserts); //Deletes all inserts
            }

            foreach(var deletion in changes.Deletes)
            {
                DataDc.GetTable(deletion.GetType()).InsertOnSubmit(deletion); //Deletes all deletions
            }
            
            var updatedtables = new List<ITable>(); //Gets all updates
            foreach( var updates in changes.Updates)
            {
                var tbl = DataDc.GetTable(updates.GetType());
                if (updatedtables.Contains(tbl)) continue;
                updatedtables.Add(tbl);
                DataDc.Refresh(RefreshMode.OverwriteCurrentValues, tbl); //Overwrites any updates to the previously saved state
            }

            _checklist = null;
            _checklist = new ObservableCheckList(DataDc); //Refreshes the _checklist variable by populating it again from the DB
            listview1.ItemsSource = _checklist;
            var view = CollectionViewSource.GetDefaultView(listview1.ItemsSource);
            view.Filter = x =>
            {
                var i = x as INSPECTION_ALERT;
                return i != null && string.Equals(i.ItemClass, _selectedItemClass);
            };//Applying the PartNumber filter again

            try
            {
                var bi = new BitmapImage(new Uri(@"\\PLY-3214\G\Mold Part Images\" + _selectedItemClass + ".bmp"));
                image1.Source = null;
                image1.Source = Utilities.DrawImage(bi, _checklist, _selectedItemClass); //Redrawing the Part Image to remove any changes

            }
            catch (Exception)
            {
                MessageBox.Show("Part Number doesn't have a conrresponding Image");
            }
            var spinstop = new Thread(Stop_Spinning);                                     //Stop Spinning Spinner
            spinstop.Start();
        }

        private void coordinates_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            //Used to select the INSPECTION_ALERT item, if it's coordinates textbox is clicked on
            listview1.SelectedItem = null;
            var newSelectedItem = (sender as TextBox).TryFindParent<ListViewItem>();
            newSelectedItem.IsSelected = true;
        }
        
        private void PriorityComboBox_DropDownOpened(object sender, EventArgs e)
        {
            listview1.SelectedItem = null;
            var newSelectedItem = (sender as ComboBox).TryFindParent<ListViewItem>();
            newSelectedItem.IsSelected = true;//Same as above. Used for selected the INSPECTION_ALERT item.
            /////////////////////////////////////////////
            var combo = (sender as ComboBox);
            if (combo != null && combo.Items.Count > 1)
                return;
            var text = Convert.ToInt32(combo.Text);
            combo.Items.Clear();
            for(var i = 1 ; i<=5 ; i++) //Adding 1-5 priorities to the dropbox so they can be selected
            {
                object obj = i;
                combo.Items.Add(obj);
                if (i == text)
                    combo.SelectedIndex = i - 1;
            }
            combo.SelectionChanged += priorityComboBox_SelectionChanged;
            combo.IsDropDownOpen = true;
        }

        private void priorityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (INSPECTION_ALERT)(((FrameworkElement) sender).DataContext);
            item.Priority = Convert.ToInt32(((ComboBox)sender).SelectedValue); //Assigning the priority to INSPECTION_ALERT item inside the binding
            try
            {
                var bi = new BitmapImage(new Uri(@"\\PLY-3214\G\Mold Part Images\" + _selectedItemClass + ".bmp"));
                image1.Source = null;
                image1.Source = Utilities.DrawImage(bi, _checklist, _selectedItemClass); //To redraw the Part Image with the new INSPECTION_ALERT priority

            }
            catch (Exception)
            {
                MessageBox.Show("Part Number doesn't have a conrresponding Image");
            }
        }

        private void moreInfo_ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            listview1.SelectedItem = null;
            var newSelectedItem = (sender as ComboBox).TryFindParent<ListViewItem>();
            newSelectedItem.IsSelected = true;
            /////////////////////////////////////////////

            var combo = (sender as ComboBox);
            if (combo != null && combo.Items.Count > 1)
                return;
            var text = combo.Text;
            combo.Items.Clear();
            foreach(var file in _qualityAlerts) //Adding all the QA's manually since no binding exists
            {
                object obj = file;
                combo.Items.Add(obj);
                if (file == text)
                    combo.SelectedItem = obj; 
            }
            combo.SelectionChanged += moreInfoComboBox_SelectionChanged;
            combo.IsDropDownOpen = true;
        }

        private void moreInfoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Setting the new data to the binding so it can be updated automatically
            var combo = (sender as ComboBox);
            var item = (INSPECTION_ALERT)(((FrameworkElement) sender).DataContext);
            if (combo != null)
            {
                item.MoreInfo = (string)combo.SelectedValue;

                if (!(((string) combo.SelectedValue).Contains("Custom") & ((string) combo.SelectedValue).Contains("Note")))
                    return;
            }
            var dialog = new CustomWindow("Enter Custom Note") {ResponseText = item.MoreInfo_Text};
            if(dialog.ShowDialog() == true)
            {
                item.MoreInfo_Text = dialog.ResponseText;
            }
        }

        private void Start_Load(object sender, SelectionChangedEventArgs e)
        {
            var spinstart = new Thread(Start_Spinning);
            spinstart.Start();
            partNumberComboBox_SelectionChanged(sender, e);
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {//Used to open the General Checklist Window.
            var app = new GeneralChecklist();
            app.ShowDialog();
        }

        private void description_GotFocus(object sender, RoutedEventArgs e)
        {//Used for selecting the INSPECTION_ALERT item when the description box is clicked on.
            listview1.SelectedItem = null;
            var newSelectedItem = (sender as TextBox).TryFindParent<ListViewItem>();
            newSelectedItem.IsSelected = true;
        }

        private void login_logout_button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(UserSignedIn))
            {
                var login = new Login();
                login.ShowDialog();
                UserSignedIn = login.Username;
                login.Close();
                if (string.IsNullOrEmpty(UserSignedIn)) return;
                user_label.Content = UserSignedIn;
                login_logout_button.Content = "Log Out";
                button_save.IsEnabled = true;
                PartNumberComboBox.IsEnabled = true;
            }
            else
            {
                login_logout_button.Content = "Log In";
                button_save.IsEnabled = false;
                PartNumberComboBox.IsEnabled = false;
                UserSignedIn = string.Empty;
                user_label.Content = string.Empty;
            }

        }

        private void troubleshoot_button_Click(object sender, RoutedEventArgs e)
        {
            var str = "1. If the inspection points on the part image don't land where you click them, change the dimensions of the part image to 455 X 160." + Environment.NewLine +
                         @"2. If a Part Image is missing or can't be found, contact the IT System Analyst to provide all new Part Images and paste them at \\PLY-3214\G\Mold Part Images location." + Environment.NewLine +
                         "3. If any more problem/bugs show up, please use the LOG file in the application folder.";
            var cw = new CustomWindow(str);
            cw.ShowDialog();
        }

        private void itemclass_Click(object sender, RoutedEventArgs e)
        {
            var itmcls = new ItemClass();
            itmcls.ShowDialog();

        }


    }
    
    public class ObservableCheckList: ObservableCollection<INSPECTION_ALERT>
    { //Allows Entity Framework to operate with bindings
        public ObservableCheckList(DataClasses1DataContext dataDc)
        {
            foreach(var alert in dataDc.INSPECTION_ALERTs)
            {
                Add(alert);
            }
        }        
    }

    public static class UiHelper
    {
        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the
        /// queried item.</param>
        /// <returns>The first parent item that matches the submitted
        /// type parameter. If not matching item can be found, a null
        /// reference is being returned.</returns>
        public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            //get parent item
            var parentObject = GetParentObject(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            var parent = parentObject as T;

            if (parent != null)
            {
                return parent;
            }
            //use recursion to proceed with next level
            return TryFindParent<T>(parentObject);
        }

        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Keep in mind that for content element,
        /// this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise
        /// null.</returns>
        public static DependencyObject GetParentObject(this DependencyObject child)
        {
            if (child == null) return null;
            //handle content elements separately
            var contentElement = child as ContentElement;

            if (contentElement != null)
            {
                var parent = ContentOperations.GetParent(contentElement);

                if (parent != null) return parent;

                var fce = contentElement as FrameworkContentElement;

                return fce != null ? fce.Parent : null;
            }
            //also try searching for parent in framework elements (such as DockPanel, etc)
            var frameworkElement = child as FrameworkElement;

            if (frameworkElement != null)
            {
                var parent = frameworkElement.Parent;
                if (parent != null) return parent;
            }
            //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }
    } 


}
