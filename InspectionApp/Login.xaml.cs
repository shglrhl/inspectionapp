﻿using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using InspectionApp.PolyconInjectionDataSet1TableAdapters;

namespace InspectionApp
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login
    {
        public string Username;
        public Login()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            var polyconInjectionDataSet1 = ((PolyconInjectionDataSet1)(FindResource("polyconInjectionDataSet1")));
            // Load data into the table USERS. You can modify this code as needed.
            var polyconInjectionDataSet1UsersPermTableAdapter = new USER_PERMISSIONSTableAdapter();
            var polyconInjectionDataSet1UsersTableAdapter = new USERSTableAdapter();
            polyconInjectionDataSet1UsersTableAdapter.Fill(polyconInjectionDataSet1.USERS);
            polyconInjectionDataSet1UsersPermTableAdapter.Fill(polyconInjectionDataSet1.USER_PERMISSIONS);
            polyconInjectionDataSet1.Tables[1].DefaultView.Sort = "USER_NAME ASC";
            
            //var uSersViewSource = ((CollectionViewSource)(FindResource("uSERSViewSource")));
            //uSersViewSource.View.MoveCurrentToFirst();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {   
            var drv = (DataRowView) uSER_NAMEComboBox.SelectedItem;
            var selectedrow = (PolyconInjectionDataSet1.USERSRow) (drv.Row);
            if (string.Equals(password_box.Password, selectedrow.USER_PASSWORD))
            {
                var permissionsRows = ((PolyconInjectionDataSet1.USERSRow) drv.Row).GetUSER_PERMISSIONSRows();
                var permissionRow = permissionsRows.First(x => x.PERMISSION_ID == "QA_INSP");
                if (permissionRow.IsNull(0))
                {
                    MessageBox.Show("You don't have permissions to access this.");
                }
                else
                {
                    Username = selectedrow.USER_NAME;
                    DialogResult = true;
                }
            }
            else
            {
                MessageBox.Show("Wrong password.");

            }
        }
    }
}
