﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InspectionApp.Linq2SQL
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="PolyconInjection")]
	public partial class DataClasses3DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertPartNumber_ItemClass(PartNumber_ItemClass instance);
    partial void UpdatePartNumber_ItemClass(PartNumber_ItemClass instance);
    partial void DeletePartNumber_ItemClass(PartNumber_ItemClass instance);
    #endregion
		
		public DataClasses3DataContext() : 
				base(global::InspectionApp.Properties.Settings.Default.PolyconInjectionConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses3DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses3DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses3DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses3DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<PartNumber_ItemClass> PartNumber_ItemClasses
		{
			get
			{
				return this.GetTable<PartNumber_ItemClass>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.PartNumber_ItemClass")]
	public partial class PartNumber_ItemClass : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _ItemClass;
		
		private string _Part_Number;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnItemClassChanging(string value);
    partial void OnItemClassChanged();
    partial void OnPart_NumberChanging(string value);
    partial void OnPart_NumberChanged();
    #endregion
		
		public PartNumber_ItemClass()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ItemClass", DbType="VarChar(10)")]
		public string ItemClass
		{
			get
			{
				return this._ItemClass;
			}
			set
			{
				if ((this._ItemClass != value))
				{
					this.OnItemClassChanging(value);
					this.SendPropertyChanging();
					this._ItemClass = value;
					this.SendPropertyChanged("ItemClass");
					this.OnItemClassChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Part_Number", DbType="VarChar(10)")]
		public string Part_Number
		{
			get
			{
				return this._Part_Number;
			}
			set
			{
				if ((this._Part_Number != value))
				{
					this.OnPart_NumberChanging(value);
					this.SendPropertyChanging();
					this._Part_Number = value;
					this.SendPropertyChanged("Part_Number");
					this.OnPart_NumberChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
